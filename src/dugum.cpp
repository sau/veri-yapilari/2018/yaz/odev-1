#include "dugum.h"

Dugum::Dugum(Ogrenci *ogrenci, Dugum *sonraki) {
    m_ogrenci = ogrenci;
    m_sonraki = sonraki;
}

Dugum::~Dugum() {
    delete m_ogrenci;
}

Ogrenci *Dugum::getOgrenci() const {
    return m_ogrenci;
}

Dugum *Dugum::getSonraki() const {
    return m_sonraki;
}

void Dugum::setSonraki(Dugum *sonraki) {
    m_sonraki = sonraki;
}
