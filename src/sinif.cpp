#include <iostream>
#include <iomanip>
#include "sinif.h"

Sinif::Sinif(unsigned int numara, unsigned int ilkOgrenciNo, size_t adetOgrenci) {
    m_numara = numara;

    m_adetOgrenci = adetOgrenci;

    int ogrenciNo = ilkOgrenciNo + m_adetOgrenci - 1; // The number of the last student in the class
    m_son = new Dugum(new Ogrenci(ogrenciNo--), nullptr); // The last student is created manually.
    m_ilk = m_son;
    // Because the one-way linked list is used, students are created from end to top.
    for (int i = m_adetOgrenci - 2; i >= 0; --i) {
        // The previous node is assigned to the newly created node as the next node.
        m_ilk = new Dugum(new Ogrenci(ogrenciNo--), m_ilk);
    }
}

Sinif::~Sinif() {
    Dugum *dugum = m_ilk;
    for (int i = 0; i < m_adetOgrenci; ++i) {
        Dugum *sil = dugum;
        dugum = dugum->getSonraki();
        delete sil;
    }
}

void Sinif::ogrenciEkle(Ogrenci *ogrenci) {
    auto dugum = new Dugum(ogrenci, nullptr); // Because this node will be appended to the end, there is no next node.

    if (++m_adetOgrenci > 1) {
        // If there are students in the class, this node is added to the last node as the next node.
        m_son->setSonraki(dugum);
    } else {
        // If there are no students in the class, this node is assigned as the first node.
        m_ilk = dugum;
    }

    // In any case, the added node will be the last node.
    m_son = dugum;
}

bool Sinif::ogrenciSil(unsigned int ogrenciNo) {
    return dugumSil(ogrenciNo);
}

bool Sinif::ogrenciVarMi(unsigned int ogrenciNo) {
    return bulDugum(ogrenciNo) != nullptr;
}

void Sinif::ekranaListele() {
    // Because line-by-line printing is required, the same node must be processed more than once. To prevent this, all
    // students are initially stored in the array.
    auto ogrenciler = new Ogrenci *[m_adetOgrenci];
    // For convenience, student numbers are also stored.
    auto ogrNolar = new int[m_adetOgrenci];

    auto dugum = m_ilk;
    for (int i = 0; i < m_adetOgrenci; ++i) {
        ogrenciler[i] = dugum->getOgrenci();
        ogrNolar[i] = ogrenciler[i]->getNumara();

        dugum = dugum->getSonraki();
    }

    // The first 2 lines, ie the class address section, are printed manually before the loop.
    std::cout << "-----------" << std::endl;
    std::cout << "|" << std::left << std::setw(9) << this << "|" << std::endl;

    // The output consists of 5 lines in total. Columns vary according to the number of students.
    for (int i = 0; i < 5; ++i) {
        // The counter starts from -1 instead of 0. In this way, different operations can be done for the class in the
        // first step and the indexes of the students are preserved.
        for (int j = -1; j < (int) m_adetOgrenci; ++j) {
            if (i % 2 == 0) {
                // Even rows specify horizontal row separators in the table.
                if (j < 0) {
                    // The first column specifies the class, hyphens are used.
                    std::cout << "-----------";
                } else {
                    // The next columns specify the students, asterisks are used.
                    std::cout << "***********";
                }
            } else if (i == 1) {
                // It is the second row of the class table and the first row of students.
                if (j < 0) {
                    // The class number is printed.
                    std::cout << std::right;
                    std::cout << "|" << std::setw(5) << m_numara << "    |";
                } else {
                    // The memory address of the student is printed.
                    std::cout << std::left;
                    std::cout << "*" << std::setw(9) << ogrenciler[j] << "*";
                }
            } else {
                if (j < 0) {
                    // The memory address of the first student in the class is printed.
                    std::cout << std::left;

                    // If there are no students in the class, it should be left blank, otherwise the first student
                    // address should be written.
                    std::ostringstream stream;
                    if (m_adetOgrenci > 0) {
                        stream << m_ilk->getOgrenci();
                    } else {
                        stream << "";
                    }
                    std::cout << "|" << std::setw(9) << stream.str() << "|";
                } else {
                    // The student's number is printed.
                    std::cout << std::right;
                    std::cout << "*" << std::setw(5) << ogrNolar[j] << "    *";
                }
            }

            std::cout << "  ";
        }

        std::cout << std::endl;
    }

    delete[] ogrenciler;
    delete[] ogrNolar;
}

unsigned int Sinif::getAdetOgrenci() const {
    return m_adetOgrenci;
}

unsigned int Sinif::getSinifNo() const {
    return m_numara;
}

bool Sinif::dugumSil(unsigned int ogrenciNo, bool temizle) {
    if (bulDugum(ogrenciNo) == nullptr) {
        return false;
    }

    return dugumSil(bulOncekiDugum(ogrenciNo), temizle);
}

bool Sinif::dugumSil(Dugum *onceki, bool temizle) {
    if (m_adetOgrenci < 1) {
        return false;
    }

    Dugum *simdiki = nullptr;

    if (onceki == nullptr) {
        // If the previous is NULL, the first node is assumed to be deleted.
        simdiki = m_ilk;
        m_ilk = m_ilk->getSonraki();
    } else {
        simdiki = onceki->getSonraki();
        if (simdiki == nullptr) {
            // The last node cannot be the previous node of any node.
            return false;
        }

        auto sonraki = simdiki->getSonraki();
        if (sonraki == nullptr) {
            // If the current node is the last node, the previous will be the last when the current node is deleted.
            m_son = onceki;
        }
        // The previous node now points the node after the current node.
        onceki->setSonraki(sonraki);
    }

    if (temizle) {
        delete simdiki;
    }

    if (--m_adetOgrenci < 1) {
        // If the last student in the class is deleted, the first and last node pointers are set to NULL.
        m_ilk = m_son = nullptr;
    }

    return true;
}

bool Sinif::ekleDugum(Dugum *onceki, Dugum *dugum) {
    if (dugum == nullptr) {
        return false;
    }

    if (onceki == nullptr) {
        dugum->setSonraki(m_ilk);
        m_ilk = dugum;
        if (m_son == nullptr) {
            m_son = m_ilk;
        }
    } else {
        if (onceki == m_son) {
            m_son = dugum;
        }
        dugum->setSonraki(onceki->getSonraki());
        onceki->setSonraki(dugum);
    }

    ++m_adetOgrenci;

    return true;
}

Dugum *Sinif::bulDugum(unsigned int ogrenciNo) {
    for (auto i = m_ilk; i != nullptr; i = i->getSonraki()) {
        if (ogrenciNo == i->getOgrenci()->getNumara()) {
            return i;
        }
    }

    return nullptr;
}

Dugum *Sinif::bulOncekiDugum(unsigned int ogrenciNo) {
    if (m_adetOgrenci < 1) {
        return nullptr;
    }

    // Since the previous node must also be stored, one variable is used to store a counter and the previous value.
    for (Dugum *i = m_ilk, *j = m_ilk->getSonraki(); j != nullptr; i = j, j = j->getSonraki()) {
        if (ogrenciNo == j->getOgrenci()->getNumara()) {
            return i;
        }
    }

    return nullptr;
}

void degistirOgrenciler(Sinif &sinif1, unsigned int ogrenciNo1, Sinif &sinif2, unsigned int ogrenciNo2) {
    auto onceki1 = sinif1.bulOncekiDugum(ogrenciNo1);
    auto dugum1 = onceki1 == nullptr ? sinif1.m_ilk : onceki1->getSonraki();
    sinif1.dugumSil(onceki1, false);

    // In case two nodes are consecutive, previous node of the second node must be taken after the first node has been
    // deleted.
    auto onceki2 = sinif2.bulOncekiDugum(ogrenciNo2);
    auto dugum2 = onceki2 == nullptr ? sinif2.m_ilk : onceki2->getSonraki();
    sinif2.dugumSil(onceki2, false);

    if (onceki1 == dugum2) {
        // If the first node is the next node of the second node.
        sinif1.ekleDugum(onceki2, dugum1);
        sinif1.ekleDugum(dugum1, dugum2);
        return;
    }

    sinif1.ekleDugum(onceki1, dugum2);
    if (onceki1 == onceki2 && &sinif1 == &sinif2) {
        // If the second node is the next node of the first node
        // Class addresses are also compared to avoid ambiguity when their previous nodes are NULL.

        sinif1.ekleDugum(dugum2, dugum1);
    } else {
        // The case where the nodes are not consecutive.
        sinif2.ekleDugum(onceki2, dugum1);
    }
}
