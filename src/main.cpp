#include <iostream>
#include "sinif.h"
#include "dugum.h"
#include "ogrenci.h"
#include "okul.h"

int main() {
    Okul *okul = new Okul(2);

    std::string hataMesaji = "Hatali bir giris yaptiniz.";

    std::string tampon; // For the standard input
    int secilen;
    bool yenidenYazdir = true; // The menu is not reprinted if there was an exception
    do {
        if (yenidenYazdir) {
            okul->ekranaListele();

            std::cout << std::endl;
            std::cout << "1...Ogrenci Degistir" << std::endl;
            std::cout << "2...Ogrenci Sil" << std::endl;
            std::cout << "3...Ogrenci Ekle" << std::endl;
            std::cout << "4...Cikis" << std::endl;
        }
        yenidenYazdir = true;

        std::cin >> tampon;

        // If the number is not integer or invalid, invalid_argument exception is thrown
        try {
            secilen = std::stoi(tampon);
            switch (secilen) {
                case 1:
                    int ogrNo1, ogrNo2;

                    std::cout << "1. ogrencinin numarasi: ";
                    std::cin >> tampon;
                    ogrNo1 = std::stoi(tampon);
                    std::cout << "2. ogrencinin numarasi: ";
                    std::cin >> tampon;
                    ogrNo2 = std::stoi(tampon);

                    okul->ogrenciDegistir(ogrNo1, ogrNo2);
                    break;
                case 2:
                    int ogrNo;

                    std::cout << "Ogrencinin numarasi: ";
                    std::cin >> tampon;
                    ogrNo = std::stoi(tampon);

                    okul->ogrenciSil(ogrNo);
                    break;
                case 3:
                    int sinifNo;

                    std::cout << "Sinif numarasi: ";
                    std::cin >> tampon;
                    sinifNo = std::stoi(tampon);

                    okul->ogrenciEkle(sinifNo);
                    break;
                case 4:
                    // This is separated because the default state specifies an exception
                    break;
                default:
                    throw std::invalid_argument("");
            }
        } catch (std::invalid_argument &e) {
            secilen = 0;
            std::cerr << hataMesaji;
            yenidenYazdir = false;
        }

        std::cout << std::endl;
    } while (secilen != 4);

    delete okul;

    return 0;
}