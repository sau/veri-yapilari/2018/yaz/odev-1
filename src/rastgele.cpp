#include "rastgele.h"

Rastgele::Rastgele(int min, int max) {
    // To increase randomness, the time since Epoch is taken in milliseconds.
    auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
    // This object is used because it produces more accurate values than rand ().
    m_engine = new std::mt19937(seed);
    // Instead of taking the mode of the value, this object is used to distribute more homogeneously into the range.
    m_distribution = new std::uniform_int_distribution<int>(min, max);
}

Rastgele::~Rastgele() {
    delete m_engine;
    delete m_distribution;
}

int Rastgele::sonraki() {
    return m_distribution->operator()(*m_engine);
}