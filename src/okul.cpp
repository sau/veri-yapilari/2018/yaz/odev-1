#include <iostream>
#include "rastgele.h"
#include "okul.h"
#include "sinif.h"

Okul::Okul(unsigned int adetSinif) {
    m_adetSinif = adetSinif;
    m_sinifNo = 1;
    m_ogrenciNo = 10;
    m_siniflar = new Sinif *[adetSinif];

    // To generate a random integer in range of [2, 6]. Specifies how many students will be in the classroom.
    Rastgele rastgele(2, 6);

    for (int i = 0; i < adetSinif; ++i) {
        m_siniflar[i] = new Sinif(m_sinifNo++, m_ogrenciNo, rastgele.sonraki());
        // The first student number in the current class is started after the last student number in the previous class.
        m_ogrenciNo += m_siniflar[i]->getAdetOgrenci();
    }
}

Okul::~Okul() {
    for (int i = 0; i < m_adetSinif; ++i) {
        delete m_siniflar[i];
    }
    delete[] m_siniflar;
}

void Okul::ogrenciEkle(unsigned int sinifNo) {
    for (int i = 0; i < m_adetSinif; ++i) {
        if (sinifNo == m_siniflar[i]->getSinifNo()) {
            m_siniflar[i]->ogrenciEkle(new Ogrenci(m_ogrenciNo++));
        }
    }
}

void Okul::ogrenciSil(unsigned int ogrenciNo) {
    for (int i = 0; i < m_adetSinif; ++i) {
        // Each class is tried until the deletion succeeds.
        if (m_siniflar[i]->ogrenciSil(ogrenciNo)) {
            return;
        }
    }
}

void Okul::ogrenciDegistir(unsigned int ogrenciNo1, unsigned int ogrenciNo2) {
    unsigned int ogrNolar[] = {ogrenciNo1, ogrenciNo2};
    bool bulundu[] = {false, false};
    Sinif *siniflar[2]; // Classes containing students

    int bulunanOgrenciSay = 0; // There should be 2 classes including students. They can be the same class.
    for (int i = 0; i < m_adetSinif && bulunanOgrenciSay < 2; ++i) {
        for (int j = 0; j < 2; ++j) {
            if (!bulundu[j]) {
                if (m_siniflar[i]->ogrenciVarMi(ogrNolar[j])) {
                    siniflar[j] = m_siniflar[i];
                    bulundu[j] = true;
                    ++bulunanOgrenciSay;
                }
            }
        }
    }

    if (bulunanOgrenciSay < 2) {
        return;
    }

    degistirOgrenciler(*siniflar[0], ogrNolar[0], *siniflar[1], ogrNolar[1]);
}

void Okul::ekranaListele() {
    for (int i = 0; i < m_adetSinif; ++i) {
        m_siniflar[i]->ekranaListele();
    }
}
