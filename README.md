# ÖDEV DOKÜMANINI DETAYLI BİR ŞEKİLDE İNCELEMEDEN ÖDEVE BAŞLAMAYIN


# VERİ YAPILARI DERSİ I. ÖDEVİ

Bu ödevde sizlerden çalıştırılabilir hali ekte verilmiş olan bir bağlı liste
uygulaması geliştirmeniz istenmektedir.

# Ödevin son teslim tarihi : 7.07.2019 24:00’a kadar

**Örnek Çalıştırılabilr Program**

https://drive.google.com/open?id=1_H7qSoJdJT3S8s637zVKV9C3_G51fPlk

**Program Giriş Ekranı**

![program-giris-ekrani](/uploads/bf28a010ba081db956c35ed7c5dc6778/image.png)

## Projede Bulunması Gereken Sınıflar


- Öğrenci
- Sınıf
- Okul
- Dugum

Her sınıf için bir başlık(.hpp) ve bir kaynak (.cpp) dosyası bulunmalıdır.

## Sınıf

![sinif](/uploads/023599f87b57dda89055dfc6baf555d3/image.png)

- Her sınıf tek yönlü bağlı liste veri yapısını kullanarak birden fazla
öğrenciyi barındırabilmektedir.
- Sınıf bir metot ile kendisi ile beraber bütün öğrencilerini aşağıdaki gibi
ekrana çıkartabilmelidir.

![sinif_cikti](/uploads/dacb9673c8b42fcddad4244e03df3839/image.png)

- Bir sınıf oluşturulduğunda 2-6 arasında öğrenci nesnesini otomatik olarak
oluşturmalı ve kendisinde bulunan bağlı listeye eklenmelidir.
- Kullanıcı istediğinde sınıfa öğrenci ekleyip çıkartabilmelidir.
- Sınıf silindiğinde sahip olduğu bütün öğrencileri serbest bırakmalıdır.

## Öğrenci

![ogrenci](/uploads/47e36c481fb01267d410fb7f99724b5f/image.png)

- Öğrenci nesnesi sadece numara değerine sahip olmalıdır.
- Öğrenci numaraları oluşturulma sırasına göre başlamalıdır.
- İlk öğrenci numarası 10 olacaktır. Sonraki oluşturulanların numaraları 11,12,13 şeklinde devam edecektir.

## Düğüm

- Öğrencileri barındıracak bağlı listenin ayrı bir düğüm sınıfı olmalıdır.
- Bu sınıf tek yönlü bağlı liste düğümü olarak kullanılabilmelidir.
- Düğüm sınıfı Öğrenci nesnesinin adresini barındırmalıdır.
- Düğüm sınıfı serbest bırakıldığında sahip olduğu öğrenci nesnesini de serbest bırakmalıdır.

## Okul

- Okul sınıfı içerisinde birden fazla Sınıf barındırabilmelidir.
- Sınıflar dinamik bir dizi içerisinde tutulmalıdır.
- Son kullanıcı okul sınıfına yazdır emrini verdiğinde sahip olunan bütün sınıflar, programın ekran görüntüsünde verildiği gibi ekrana çıkartılmalıdır.
- Son kullanıcı öğrenci ekleme, silme ve değiştirme işlemlerini okul
nesnesine yaptırmalıdır.

# UYULMASI GEREKEN KOD DÜZENİ

Her C++ dosyasının başında aşağıdaki yorum bloğu bulunacaktır. Yorum bulunmayan her C++
dosyası için 10 puan kırılacaktır. ( **pdf üzerinden kopyalayıp yapıştırmanız problem çıkartabilir** )

![yorum-blogu](/uploads/41fcded798fd869927eed3f199897e21/image.png)

# ÖDEV TESLİM KURALLARI

## Ödev Klasörü Schoology Sistemine Yüklenecektir.

- Ödev klasörü zip formatında sıkıştırılarak Schoology sistemine yüklenmesi gerekmektedir.

## ÖDEV İÇERİSİNDE TÜRKÇE KARAKTER BULUNMAMALI

- (YORUMLAR DA DAHİL)

## DERLEYİCİ VE TESLİM TARİHİ

- Programlarınız derlenirken, GNU Mingw derleyicisi kullanılacaktır.
- Projeniz içerisinde Makefile dosyası bulunmalı ve derleme işlemini
gerçekleştirmelidir.
- Projenizin klasör hiyerarşisi aşağıdaki gibi olmalıdır.

**Zamanında teslim edilmeyen ödevler değerlendirilmeyecektir.**


( En ufak bir gecikme **ödevin kabul edilmemesi ile sonuçlanır)**

# KOD PAYLAŞMAK YASAKTIR

Ödevler bireyseldir ve verilen bütün ödevler karşılaştırılacaktır. Birbirine çok
benzeyen ödevler **kopya** muamelesi görecektir. Öğretim üyesi kopya durumunda
ödevi değerlendirmez veya gerekli soruşturma ve ceza işlemlerini başlatabilir.