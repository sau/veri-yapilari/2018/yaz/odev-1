CC=g++
BIN=bin
INCL=include
LIB=lib
SRC=src
OUT=main

.PHONY: makedirs clean

all: makedirs compile run

makedirs:
	IF NOT EXIST $(BIN) (MD "$(BIN)")
	IF NOT EXIST $(INCL) (MD "$(INCL)")
	IF NOT EXIST $(LIB) (MD "$(LIB)")
	IF NOT EXIST $(SRC) (MD "$(SRC)")

compile: main.o dugum.o ogrenci.o okul.o rastgele.o sinif.o
	$(CC) -o $(BIN)/$(OUT) $(LIB)/main.o $(LIB)/dugum.o $(LIB)/ogrenci.o $(LIB)/okul.o $(LIB)/rastgele.o $(LIB)/sinif.o

run:
	$(BIN)/$(OUT).exe
	
main.o: $(SRC)/main.cpp $(INCL)/sinif.h $(INCL)/dugum.h $(INCL)/ogrenci.h $(INCL)/dugum.h $(INCL)/ogrenci.h $(INCL)/okul.h
	$(CC) -I $(INCL) -c $(SRC)/main.cpp -o $(LIB)/main.o

dugum.o: $(SRC)/dugum.cpp $(INCL)/dugum.h $(INCL)/ogrenci.h
	$(CC) -I $(INCL) -c $(SRC)/dugum.cpp -o $(LIB)/dugum.o

ogrenci.o: $(SRC)/ogrenci.cpp $(INCL)/ogrenci.h
	$(CC) -I $(INCL) -c $(SRC)/ogrenci.cpp -o $(LIB)/ogrenci.o

okul.o: $(SRC)/okul.cpp $(INCL)/rastgele.h $(INCL)/okul.h $(INCL)/sinif.h $(INCL)/dugum.h $(INCL)/ogrenci.h
	$(CC) -I $(INCL) -c $(SRC)/okul.cpp -o $(LIB)/okul.o

rastgele.o: $(SRC)/rastgele.cpp $(INCL)/rastgele.h
	$(CC) -I $(INCL) -c $(SRC)/rastgele.cpp -o $(LIB)/rastgele.o

sinif.o: $(SRC)/sinif.cpp $(INCL)/sinif.h $(INCL)/dugum.h $(INCL)/ogrenci.h
	$(CC) -I $(INCL) -c $(SRC)/sinif.cpp -o $(LIB)/sinif.o

clean:
	DEL "$(BIN)\$(OUT).exe"
	DEL "$(LIB)\*.o"