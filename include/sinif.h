#ifndef SINIF_H
#define SINIF_H


#include "dugum.h"

class Sinif {
private:
    Dugum *m_ilk;
    Dugum *m_son;
    unsigned int m_adetOgrenci;
    unsigned int m_numara;

    /**
     *
     * @param ogrenciNo Number of the student to delete
     * @param temizle if true the node is deleted, false otherwise
     * @return true if the result is successful, false if not
     */
    bool dugumSil(unsigned int ogrenciNo, bool temizle = true);

    /**
     *
     * @param onceki The previous node of the node to be deleted
     * @param temizle if true the node is deleted, false otherwise
     * @return true if the result is successful, false if not
     */
    bool dugumSil(Dugum *onceki, bool temizle = true);

    bool ekleDugum(Dugum *onceki, Dugum *dugum);

    Dugum *bulDugum(unsigned int ogrenciNo);

    Dugum *bulOncekiDugum(unsigned int ogrenciNo);

public:
    explicit Sinif(unsigned int numara, unsigned int ilkOgrenciNo, size_t adetOgrenci);

    ~Sinif();

    void ogrenciEkle(Ogrenci *ogrenci);

    bool ogrenciSil(unsigned int ogrenciNo);

    bool ogrenciVarMi(unsigned int ogrenciNo);

    void ekranaListele();

    unsigned int getAdetOgrenci() const;

    unsigned int getSinifNo() const;

    /**
     * Swap positions of two students
     * @param sinif1 First student's class
     * @param ogrenciNo1 First student
     * @param sinif2 Second student's class
     * @param ogrenciNo2 Second student
     */
    friend void degistirOgrenciler(Sinif &sinif1, unsigned int ogrenciNo1, Sinif &sinif2, unsigned int ogrenciNo2);
};


#endif //SINIF_H
