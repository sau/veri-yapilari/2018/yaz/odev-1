#ifndef OKUL_H
#define OKUL_H


#include <cstddef>
#include "sinif.h"

class Okul {
private:
    Sinif **m_siniflar;
    unsigned int m_sinifNo;
    unsigned int m_ogrenciNo;
    size_t m_adetSinif;
public:
    explicit Okul(unsigned int adetSinif);

    ~Okul();

    void ogrenciEkle(unsigned int sinifNo);

    void ogrenciSil(unsigned int ogrenciNo);

    void ogrenciDegistir(unsigned int ogrenciNo1, unsigned int ogrenciNo2);

    void ekranaListele();
};


#endif //OKUL_H
