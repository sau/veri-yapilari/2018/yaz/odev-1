#ifndef OGRENCI_H
#define OGRENCI_H


class Ogrenci {
private:
    unsigned int m_numara;
public:
    explicit Ogrenci(unsigned int numara);

    unsigned int getNumara() const;
};


#endif //OGRENCI_H
