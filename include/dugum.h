#ifndef DUGUM_H
#define DUGUM_H


#include "ogrenci.h"

class Dugum {
private:
    Ogrenci *m_ogrenci;
    Dugum *m_sonraki;
public:
    Dugum(Ogrenci *ogrenci, Dugum *sonraki);

    ~Dugum();

    Ogrenci *getOgrenci() const;

    Dugum *getSonraki() const;

    void setSonraki(Dugum *sonraki);
};


#endif //DUGUM_H
