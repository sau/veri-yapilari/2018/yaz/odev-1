#ifndef RASTGELE_H
#define RASTGELE_H


#include <chrono>
#include <random>

class Rastgele {
private:
    std::mt19937 *m_engine;
    std::uniform_int_distribution<int> *m_distribution;
public:
    Rastgele(int min, int max);

    ~Rastgele();

    int sonraki();
};


#endif
